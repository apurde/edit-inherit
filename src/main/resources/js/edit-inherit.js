function EditInherit() {
	
	var status = {};
	var applyClicked = false;
	var statusForm = ' \
		<form class="aui top-label"> \
			<div id="edit-inherit-control-div">\
				<div id="edit-inherit-toggle-div">\
					<aui-toggle label="toggleTitle" id="edit-inherit-toggle" checked="true"></aui-toggle>\
					<span>toggleTitle</span>\
				</div>\
				<div id="edit-inherit-options-div">\
        			<select class="select long-field" id="edit-inherit-options-select" multiple="">\
					</select>\
				</div>\
				<div id="edit-inherit-help-div">\
					<a href="https://bitbucket.org/apurde/edit-inherit/wiki" target="_blank"><span class="aui-icon aui-icon-small aui-iconfont-help"></span></a>\
				</div>\
			</div>\
		</form>\
		<div id="edit-inherit-hint"></div>';
	
	
	this.init = function() {
		getStatus(afterStatusLoaded);
	};
	
	
	// get the edit permission inheritance status
	var getStatus = function(callback) {
		var pageId = AJS.Meta.get("page-id");
		if(pageId) {
			AJS.$.ajax({
				url: AJS.contextPath() + "/rest/editinherit/1.0/" + pageId + "/getStatus",
				type: "GET",
				dataType: "json",
				success: function(result) {
					status = result;
					callback();
				}
			});
		}
	};
	
	
	// things to do once we have the status
	var afterStatusLoaded = function() {
		if(status.canSetPermission) {
			AJS.$("#content-metadata-page-restrictions, #rte-button-restrictions, #editPageLink, #action-page-permissions-link").click(function() {
				startObserver();
			});
		}
		if(!status.canAdd) {
			AJS.$("#quick-create-page-button, #create-page-button").off("click");
			AJS.$("#quick-create-page-button, #create-page-button").click(function() {
				AJS.flag({
					type: 'error',
					body: AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.cantadd")
				});
				return false;
			});
		}
	};
	
	
	// start the mutation observer
	var startObserver = function() {
		console.log("Starting Edit Permission Inherit observer");
		editInheritObserver.observe(document.body, editInheritObserverConfig);
	};
	
	
	// stop the mutation observer
	var stopObserver = function() {
		console.log("Stopping Edit Permission Inherit observer");
		editInheritObserver.disconnect();
	};
	
	
	// the mutation observer config
	var editInheritObserverConfig = {
		childList : true,
		subtree : true,
	};
	
	
	// the mutation observer
	var editInheritObserver = new MutationObserver(function(mutations) {
		mutations.forEach(function(mutation) {
			mutation.addedNodes.forEach(function(node) {		
				var id = AJS.$(node).attr('id') || "";
				if(id.indexOf("update-page-restrictions-dialog") > -1) {
					getStatus(addEditInheritInformation);
				}
			});
			mutation.removedNodes.forEach(function(node) {		
				var id = AJS.$(node).attr('id') || "";
				if(id.indexOf("update-page-restrictions-dialog") > -1) {
					dialogClose();
				}
			});
		});
	});
	
	
	// add the edit permission inheritance information to the dialog
	var addEditInheritInformation = function() {
		
		if(AJS.Meta.get("page-id") == "0" || AJS.$("#edit-inherit-control-div").length > 0) { return; }
		
		var prefilledStatusForm = statusForm.replace(/toggleTitle/g, AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.toggle"));
		if(!(status.mode & 1)) {
			prefilledStatusForm = prefilledStatusForm.replace(' checked="true"', "");
		}
        AJS.$("#update-page-restrictions-dialog").find(".aui-dialog2-content").append(prefilledStatusForm);
        AJS.$(".page-restrictions-dialog-entities-container").css("overflow", "auto");
		
		AJS.$("#edit-inherit-options-select").append('<option value="2">' + AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.option.hard") + '</option>');
		AJS.$("#edit-inherit-options-select").append('<option value="4">' + AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.option.delete") + '</option>');
		AJS.$("#edit-inherit-options-select").append('<option value="8">' + AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.option.creator") + '</option>');
		AJS.$("#edit-inherit-options-select").append('<option value="16">' + AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.option.delta") + '</option>');
		
		AJS.$("#edit-inherit-options-select").auiSelect2({
			placeholder: AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.options")
		});
		modeToElements(status.mode);
		
		AJS.$("#edit-inherit-options-select, #edit-inherit-toggle").on("change", function() {
			if(calculateMode() != status.mode) {
				enableButton(AJS.$("#page-restrictions-dialog-save-button"));
			}
		});
		
		AJS.$("#page-restrictions-dialog-save-button").click(function() {
			applyClicked = true;
			status.mode = calculateMode();
		});
		
		if(status.closestInheritanceId != 0) {
			AJS.$("#edit-inherit-hint").html(AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.covered").replace("$pageLink", AJS.contextPath() + "/pages/viewpage.action?pageId=" + status.closestInheritanceId));
		}	
		
		if(status.inheritanceRunning == true) {
			AJS.$("#edit-inherit-hint").html(AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.running"));			
		}
	};
	
	
	// calculate the mode from the elements
	var calculateMode = function() {
		if(AJS.$("#edit-inherit-toggle").prop("checked")) {
			var status = 1;
			var selectedOptions = AJS.$("#edit-inherit-options-select").val() || [];
			for(var n = 0; n < selectedOptions.length; n++) {
				status += parseInt(selectedOptions[n], 10);
			}
			return status;
		}
		else {
			return 0;
		}
	};
	
	
	// init the elements based on the mode value
	var modeToElements = function(mode) {
		if(mode & 1) { AJS.$("#edit-inherit-toggle").prop("checked", true);	}
		var options = [];
		if(mode & 2) { options.push("2"); }
		if(mode & 4) { options.push("4"); }	
		if(mode & 8) { options.push("8"); }	
		if(mode & 16) { options.push("16"); }	
		AJS.$("#edit-inherit-options-select").val(options).trigger("change");
	}
	
	
	// enable a button
	// remark: we needed to do this manually as the enable function has been replaced in Confluence 7.0
	var enableButton = function(button) {
		AJS.$(button).removeClass("disabled");
		AJS.$(button).removeAttr("disabled");
		AJS.$(button).attr("aria-disabled", "false");
	};
	
	
	// when the dialog has been closed
	var dialogClose = function() {
		if(applyClicked == true) {
			applyClicked = false;
			setStatus();
		}
		stopObserver();
	};
	
	
	// communicate the edit permission inheritance mode to the server
	var setStatus = function() {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/editinherit/1.0/" + AJS.Meta.get("page-id") + "/set?mode=" + status.mode,
			type: "PUT",
			dataType: "json",
			success: function(result) {
				if(result.status == "blocked") {
					AJS.flag({
						type: 'error',
						body: AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.blocked")
					});	
					getStatus(function() {});
				}
			},
			error: function(request, status, error) {
				AJS.flag({
					type: 'error',
					body: AJS.I18n.getText("de.edrup.confluence.plugins.edit-inherit.error")
				});		
				getStatus(function() {});
			}
		});
	};
}


var editInherit = new EditInherit();

AJS.toInit(function() {
	editInherit.init();
});