package de.edrup.confluence.plugins.editinherit.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.core.DefaultDeleteContext;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.security.ContentPermissionSet;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.user.GroupManager;

import de.edrup.confluence.plugins.editinherit.ao.Inheritance;
import de.edrup.confluence.plugins.editinherit.ao.InheritanceService;


@Named
public class EditInheritHelper {
	
	private final InheritanceService inheritanceService;
	private final PageManager pageMan;
	private final TransactionTemplate transactionTemplate;
	private final ContentPermissionManager contentPermissionMan;
	private final PermissionManager permissionMan;
	private final PluginLicenseManager licenseMan;
	private final Cache<String, Long> conflictingInheritances;
	private final Cache<Long, Long> incompleteInheritances;
	private final GroupManager groupMan;
	private final UserAccessor userAcc;
	
	private static ExecutorService pool = Executors.newFixedThreadPool(4);  
	private static final Logger log = LoggerFactory.getLogger(EditInheritHelper.class);
	
	private final int APPLY_DELAY = 3000;
	private final int ADD_DELAY = 3000;
	private final Long MAX_BLOCKING_TIME = 60000L;

	private final int HARD_FLAG = 0x02;
	private final int DELETE_FLAG = 0x04;
	private final int CREATOR_FLAG = 0x08;
	private final int DELTA_FLAG = 0x10;
	
	
	@Inject
	public EditInheritHelper(InheritanceService inheritanceService, @ComponentImport PageManager pageMan,
		@ComponentImport TransactionTemplate transactionTemplate, @ComponentImport ContentPermissionManager contentPermissionMan,
		@ComponentImport PermissionManager permissionMan, @ComponentImport PluginLicenseManager licenseMan,
		@ComponentImport CacheManager cacheMan, @ComponentImport GroupManager groupMan, @ComponentImport UserAccessor userAcc) {
		this.inheritanceService = inheritanceService;
		this.pageMan = pageMan;
		this.transactionTemplate = transactionTemplate;
		this.contentPermissionMan = contentPermissionMan;
		this.permissionMan = permissionMan;
		this.licenseMan = licenseMan;
		this.groupMan = groupMan;
		this.userAcc = userAcc;
		conflictingInheritances = cacheMan.getCache("Edit Permission Inheritance conflicting cache", null, new CacheSettingsBuilder().expireAfterAccess(10, TimeUnit.MINUTES).build());
		incompleteInheritances = cacheMan.getCache("Edit Permission Inheritance incomplete cache", null, new CacheSettingsBuilder().expireAfterAccess(4, TimeUnit.HOURS).build());
	}
	
	
	public String getStatus(Long pageId) {
		
		JSONObject status = new JSONObject();
		
		boolean hasEditPermission = contentPermissionMan.hasContentLevelPermission(AuthenticatedUserThreadLocal.get(), ContentPermission.EDIT_PERMISSION, pageMan.getPage(pageId));
		boolean hasSetPermissionsPermission = permissionMan.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.SET_PERMISSIONS, pageMan.getPage(pageId));
		
		if(inheritanceService.hasInheritance(pageId)) {
			int mode =  inheritanceService.getInheritance(pageId).getMode();
			status.put("mode", mode);
			status.put("canAdd", ((mode & DELETE_FLAG) == 0) || hasEditPermission);
			status.put("closestInheritanceId", 0L);
			status.put("inheritanceRunning", isInheritanceRunning(pageId));
		}
		
		else {
			status.put("mode", 0);
			Inheritance closestInheritance = getClosestInheritance(pageId);
			if(closestInheritance != null) {
				status.put("canAdd", ((closestInheritance.getMode() & DELETE_FLAG) == 0) || hasEditPermission);
				status.put("closestInheritanceId", closestInheritance.getPageId());
			}
			else {
				status.put("canAdd", true);
				status.put("closestInheritanceId", 0L);
			}
		}
		
		status.put("canSetPermission", hasSetPermissionsPermission && isLicensed());
		
		return status.toString();
	}
		
	
	public boolean applyInheritance(Long pageId, Integer mode) {
		
		if(isConflictingInheritanceRunning(pageId)) {
			return false;
		}
				
		if(mode > 0) {
			log.debug("Applying inheritance on page {} with mode {}", pageId, mode);
			registerInheritance(pageId);
			inheritanceService.addOrUpdateInheritance(pageId, mode, inheritanceService.hasInheritance(pageId) ? inheritanceService.getInheritance(pageId).getEditPermissions() : "none");
			String userKey = getCurrentUserKey();
			CompletableFuture.supplyAsync(() -> sleepAndExecuteInheritance(pageId, mode, userKey), pool);
		}
		else {
			log.debug("Removing inheritance on page {}", pageId);
			inheritanceService.removeInheritance(pageId);
		}
		
		return true;
	}
	
	
	public void handleAdd(Long pageId) {
		Long tick = new Date().getTime();
		String userKey = getCurrentUserKey();
		CompletableFuture.supplyAsync(() -> sleepAndHandleAddAndMove(pageId, true, tick, userKey));
	}
	
	
	public void handleMove(Long pageId) {
		Long tick = new Date().getTime();
		String userKey = getCurrentUserKey();
		CompletableFuture.supplyAsync(() -> sleepAndHandleAddAndMove(pageId, false, tick, userKey));
	}

	
	public void handlePageRemove(Long pageId) {
		transactionTemplate.execute(() -> inheritanceService.removeInheritance(pageId));
	}
	
	
	public JSONArray getAffectedPages() {
		return transactionTemplate.execute(() -> getAffectedPagesI());
	}
	
	
	public JSONArray getAllInheritances(String spaceKeys) {
		return transactionTemplate.execute(() -> getAllInheritancesI(spaceKeys));
	}
	
	
	private boolean isLicensed() {
		return licenseMan.getLicense().isDefined() ? licenseMan.getLicense().get().isValid() : false;
	}
	
	
	private boolean isConflictingInheritanceRunning(Long pageId) {
		Long timeStamp = conflictingInheritances.get(pageMan.getPage(pageId).getSpace().getKey());
		if(timeStamp != null) {
			return ((now() - timeStamp) < MAX_BLOCKING_TIME) ? true : false;
		}
		return false;
	}
	
	
	private boolean isInheritanceRunning(Long pageId) {
		Long timeStamp = incompleteInheritances.get(pageId);
		return timeStamp != null;
	}
	
	
	private void registerInheritance(Long pageId) {
		conflictingInheritances.put(pageMan.getPage(pageId).getSpace().getKey(), now());
		incompleteInheritances.put(pageId, now());
	}
	
	
	private boolean unregisterInheritance(Long pageId) {
		conflictingInheritances.remove(pageMan.getPage(pageId).getSpace().getKey());
		incompleteInheritances.remove(pageId);
		return true;
	}
	
	
	private Long now() {
		Date d = new Date();
		return d.getTime();
	}

		
	private boolean sleepAndExecuteInheritance(Long pageId, Integer mode, String userKey) {
		try {
			setCurrentUserByKey(userKey);
			
			log.debug("Staring edit permission inheritance for page {} with mode {}", pageId, mode);
			Thread.sleep(APPLY_DELAY);
			List<List<Long>> batches = transactionTemplate.execute(() -> SplitList.partition(getDescendantIdsNotCovered(pageId), 30));
			log.debug("Executing in {} batches", batches.size());
			for(List<Long> batch : batches) {
				transactionTemplate.execute(() -> inheritEditPermissions(pageId, batch, mode, isSet(mode, DELTA_FLAG)));
			}
			transactionTemplate.execute(() -> inheritanceService.addOrUpdateInheritance(pageId, mode, getCSPermissionString(pageId)));
			transactionTemplate.execute(() -> unregisterInheritance(pageId));
			log.debug("Edit permission inheritance completed");
			return true;
		}
		catch(Exception e) {
			log.error(getStackTrace(e));
			return false;
		}
	}
	
	
	private List<Long> getDescendantIdsNotCovered(Long pageId) {
		List<Long> descIds = pageMan.getDescendantIds(pageMan.getPage(pageId)).stream().collect(Collectors.toList());
		List<Long> subInheritanceIds = inheritanceService.getInheritagesOfList(descIds);
		for(Long subInheritanceId : subInheritanceIds) {
			descIds.removeAll(pageMan.getDescendantIds(pageMan.getPage(subInheritanceId)));
		}
		descIds.removeAll(subInheritanceIds);
		return descIds;
	}
	
	
	private boolean inheritEditPermissions(Long parentPageId, List<Long> pageIds, Integer mode, boolean respectLast) {
		
		List<ContentPermission> parentPermissions = getEditPermissions(parentPageId);
		String lastParentPermissions = inheritanceService.hasInheritance(parentPageId) ? inheritanceService.getInheritance(parentPageId).getEditPermissions() : "";
		String[] lastPermissionStrings = lastParentPermissions.split(",");
		List<String> currentPermissionStrings = parentPermissions.stream().map(x -> getPermissionString(x)).collect(Collectors.toList());

		for(ContentPermission parentPermission : parentPermissions) {
			if(!lastParentPermissions.contains(getPermissionString(parentPermission)) || !respectLast) {
				log.debug("Adding permission {} to all {} pages in that batch", getPermissionString(parentPermission), pageIds.size());
				for(Long pageId : pageIds) {
					addContentPermissionIfMissing(parentPermission, pageId);
				}
			}
		}
		
		for(String lastPermissionString : lastPermissionStrings) {
			if(!currentPermissionStrings.contains(lastPermissionString) && !lastPermissionString.isEmpty() && !lastPermissionString.equals("none")) {
				log.debug("Removing permission {} from all {} pages in that batch", lastPermissionString, pageIds.size());
				for(Long pageId : pageIds) {
					findContentPermissionAndDeleteIt(lastPermissionString, pageId);
				}
			}
		}
		
		if(isSet(mode, HARD_FLAG) || parentPermissions.isEmpty()) {
			for(Long pageId : pageIds) {
				List<ContentPermission> childPermissions = getEditPermissions(pageId);
				for(ContentPermission childPermission : childPermissions) {
					if(!currentPermissionStrings.contains(getPermissionString(childPermission))) {
						log.debug("Removing {} from page {} due to HARD mode or empty parent edit permissions", getPermissionString(childPermission), pageId);
						contentPermissionMan.removeContentPermission(childPermission);
					}
				}
			}
		}
		
		if(isSet(mode, CREATOR_FLAG) && !parentPermissions.isEmpty()) {
			for(Long pageId : pageIds) {
				ConfluenceUser creator = pageMan.getPage(pageId).getCreator();
				if(!currentPermissionStrings.contains(creator.getKey().getStringValue())) {
					addContentPermissionIfMissing(ContentPermission.createUserPermission(ContentPermission.EDIT_PERMISSION, creator), pageId);
				}
			}
		}
		
		return true;
	}
	
	
	private boolean isSet(int mode, int flag) {
		return (mode & flag) > 0;
	}
	
	
	private List<ContentPermission> getEditPermissions(Long pageId) {
		List<ContentPermissionSet> permissionSets = contentPermissionMan.getContentPermissionSets(pageMan.getPage(pageId), ContentPermission.EDIT_PERMISSION);
		ContentPermissionSet permissionSet = permissionSets.isEmpty() ? new ContentPermissionSet() : permissionSets.get(0);
		return new ArrayList<ContentPermission>(permissionSet.getAllExcept(new ArrayList<ContentPermission>()));
	}
	
	
	private List<ContentPermission> getViewPermissions(Long pageId) {
		List<ContentPermissionSet> permissionSets = contentPermissionMan.getContentPermissionSets(pageMan.getPage(pageId), ContentPermission.VIEW_PERMISSION);
		
		ContentPermissionSet directPermissionSet = null;
		for(ContentPermissionSet permissionSet : permissionSets) {
			if(permissionSet.getOwningContent() != null && permissionSet.getOwningContent().getId() == pageId) {
				directPermissionSet = permissionSet;
			}
		}
		
		if(directPermissionSet == null) {
			directPermissionSet = new ContentPermissionSet();
		}
		
		return new ArrayList<ContentPermission>(directPermissionSet.getAllExcept(new ArrayList<ContentPermission>()));
	}
	
		
	private String getPermissionString(ContentPermission permission) {
		if(permission.isGroupPermission()) {
			return "g:" + permission.getGroupName();
		}
		else {
			return "u:" + permission.getUserSubject().getKey().toString();
		}
	}
	
	
	private void findContentPermissionAndDeleteIt(String permissionString, Long pageId) {
		List<ContentPermission> permissions = getEditPermissions(pageId);
		for(ContentPermission permission : permissions) {
			if(permission.isGroupPermission() && permissionString.startsWith("g:") && permissionString.replace("g:", "").equals(permission.getGroupName())) {
				contentPermissionMan.removeContentPermission(permission);
				return;
			}
			if(permission.isUserPermission() && permissionString.startsWith("u:") && permissionString.replace("u:", "").equals(permission.getUserSubject().getKey().getStringValue())) {
				contentPermissionMan.removeContentPermission(permission);
				return;
			}			
		}
	}
	
	
	private void addContentPermissionIfMissing(ContentPermission cloneFrom, Long pageId) {
		if(!getEditPermissions(pageId).contains(cloneFrom)) {
			if(hasViewPermission(cloneFrom, pageId)) {
				contentPermissionMan.addContentPermission(safeClonePermission(cloneFrom), pageMan.getPage(pageId));
			}
			else {
				if(cloneFrom.isGroupPermission()) {
					log.info("Skipping edit permission inheritance on page {} as the group {} does not have a view permission here", pageId, cloneFrom.getGroupName());
				}
				else {
					log.info("Skipping edit permission inheritance on page {} as the user {} does not have a view permission here", pageId, cloneFrom.getUserSubject().getName());					
				}
			}
		}
	}
	

	private ContentPermission safeClonePermission(ContentPermission permission) {
		return permission.isGroupPermission() ?
			ContentPermission.createGroupPermission(ContentPermission.EDIT_PERMISSION, permission.getGroupName()) :
			ContentPermission.createUserPermission(ContentPermission.EDIT_PERMISSION, permission.getUserSubject());	
	}
	
	
	private boolean hasViewPermission(ContentPermission permission, Long pageId) {
		List<ContentPermission> viewPermissions = getViewPermissions(pageId);
		if(viewPermissions.isEmpty()) {
			return true;
		}
		if(permission.isUserPermission()) {
			List<String> groupNames = new ArrayList<String>();
			try {
				groupNames.addAll(StreamSupport.stream(groupMan.getGroups(permission.getUserSubject()).spliterator(), false).map(x->x.getName()).collect(Collectors.toList()));
			}
			catch(Exception e) {
				log.error("Could not determine group names: {}", e.toString());
			}
			for(ContentPermission viewPermission : viewPermissions) {
				if(viewPermission.isUserPermission()) {
					if(viewPermission.getUserSubject().equals(permission.getUserSubject())) {
						return true;
					}
				}
				else {
					if(groupNames.contains(viewPermission.getGroupName())) {
						return true;
					}
				}
			}
			return false;
		}
		else {
			for(ContentPermission viewPermission : viewPermissions) {
				if(viewPermission.isGroupPermission()) {
					if(viewPermission.getGroupName().equals(permission.getGroupName())) {
						return true;
					}
				}
			}
			return false;
		}
	}
	
	
	private String getCSPermissionString(Long pageId) {
		return String.join(",", getEditPermissions(pageId).stream().map(x -> getPermissionString(x)).collect(Collectors.toList()));
	}
	
	
	private boolean sleepAndHandleAddAndMove(Long pageId, boolean isAdd, Long tick, String userKey) {
		try {
			setCurrentUserByKey(userKey);
			
			Long elapsed = new Date().getTime() - tick;
			log.debug("Add or move of page {} detected {} ms ago", pageId, elapsed);
			
			if(elapsed < ADD_DELAY) {
				Thread.sleep(ADD_DELAY - elapsed);
			}
			
			if(transactionTemplate.execute(() -> inheritanceService.hasInheritance(pageId))) {
				log.debug("Parent page of added branch holds inheritance - nothing to do");
				return true;
			}
			
			Inheritance closestInheritance = transactionTemplate.execute(() -> getClosestInheritance(pageId));
			if(closestInheritance != null) {
				log.debug("Found closest inheritance on page {}", closestInheritance.getPageId());
				
				List<Long> descendantIds = transactionTemplate.execute(() -> getDescendantIdsNotCovered(pageId));
				descendantIds.add(pageId);
				List<List<Long>> batches = SplitList.partition(descendantIds, 30);
				log.debug("Executing in {} batches", batches.size());
				
				for(List<Long> batch : batches) {
					transactionTemplate.execute(() -> inheritEditPermissions(closestInheritance.getPageId(), batch, closestInheritance.getMode(), false));
				}
				
				if(isAdd && ((closestInheritance.getMode() & DELETE_FLAG) == 0)) {
					transactionTemplate.execute(() -> ensureEditRightsOfCreator(pageId));
				}
				
				if(isAdd && ((closestInheritance.getMode() & DELETE_FLAG) > 0)) {
					transactionTemplate.execute(() -> trashPageIfNoEditRights(pageId));
				}
			}
			return true;
		}
		catch(Exception e) {
			log.error(getStackTrace(e));
			return false;
		}
	}
	
	
	private Inheritance getClosestInheritance(Long pageId) {
		List<Long> ancestorIds = getAncestorIds(pageId);
		List<Long> inheritancesOnAncestors = inheritanceService.getInheritagesOfList(ancestorIds);
		Collections.sort(inheritancesOnAncestors, new Comparator<Long>() {
		    public int compare(Long left, Long right) {
		        return Integer.compare(ancestorIds.indexOf(left), ancestorIds.indexOf(right));
		    }
		});
		
		return inheritancesOnAncestors.isEmpty() ? null : inheritanceService.getInheritance(inheritancesOnAncestors.get(inheritancesOnAncestors.size() - 1));
	}
	
	
	private List<Long> getAncestorIds(Long pageId) {
		return pageMan.getPage(pageId).getAncestors().stream().map(x -> x.getId()).collect(Collectors.toList());
	}

		
	private boolean ensureEditRightsOfCreator(Long pageId) {
		if(!contentPermissionMan.hasContentLevelPermission(pageMan.getPage(pageId).getCreator(), ContentPermission.EDIT_PERMISSION, pageMan.getPage(pageId))) {
			contentPermissionMan.addContentPermission(ContentPermission.createUserPermission(ContentPermission.EDIT_PERMISSION, pageMan.getPage(pageId).getCreator()), pageMan.getPage(pageId));
			log.debug("Adding edit permssion of creator to page {}", pageId);
		}
		return true;
	}
	
	
	private boolean trashPageIfNoEditRights(Long pageId) {
		if(!contentPermissionMan.hasContentLevelPermission(pageMan.getPage(pageId).getCreator(), ContentPermission.EDIT_PERMISSION, pageMan.getPage(pageId))) {
			pageMan.trashPage(pageMan.getPage(pageId), DefaultDeleteContext.builder().build());
			log.debug("Newly added page {} had to be trashed due to insufficient permissions", pageId);
		}
		return true;		
	}
	
	
	private JSONArray getAffectedPagesI() {
		JSONArray results = new JSONArray();
		
		for(Inheritance inheritance : inheritanceService.getAllInheritances()) {
			JSONObject result = new JSONObject();
			result.put("inheritanceRoot", inheritance.getPageId());
			result.put("active", (inheritance.getMode() & 0x01) > 0);
			List<Long> descIds = pageMan.getDescendantIds(pageMan.getPage(inheritance.getPageId())).stream().collect(Collectors.toList());
			List<Long> affectedDescendants = new ArrayList<Long>();
			for(Long descId : descIds) {
				List<String> editPermissions = getEditPermissions(descId).stream().map(x -> getPermissionString(x)).collect(Collectors.toList());
				List<String> viewPermissions = getViewPermissions(descId).stream().map(x -> getPermissionString(x)).collect(Collectors.toList());
				boolean affected = false;
				if(!viewPermissions.isEmpty()) {
					for(String editPermission : editPermissions) {
						if(!viewPermissions.contains(editPermission)) {
							affected = true;
						}
					}
				}
				if(affected) {
					affectedDescendants.add(descId);
				}
			}
			result.put("affectedPageIds", affectedDescendants);
			results.put(result);
		}
			
		return results;
	}
	
	
	private JSONArray getAllInheritancesI(String spaceKeys) {
		JSONArray results = new JSONArray();
		List<String> spaceKeysList = Arrays.asList(spaceKeys.split(","));
		
		for(Inheritance inheritance : inheritanceService.getAllInheritances()) {
			JSONObject result = new JSONObject();
			result.put("pageId", inheritance.getPageId());
			result.put("active", (inheritance.getMode() & 0x01) > 0);
			result.put("mode", inheritance.getMode());
			
			Page p = pageMan.getPage(inheritance.getPageId());
			if(p != null) {
				result.put("title", p.getTitle());
				result.put("spaceKey", p.getSpaceKey());
			}
			
			if(spaceKeys.isEmpty() || spaceKeysList.contains(result.optString("spaceKey"))) {
				results.put(result);
			}
		}
		return results;
	}
	
	
	private String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}	
	
	
	private String getCurrentUserKey() {
		return AuthenticatedUserThreadLocal.get() != null ? AuthenticatedUserThreadLocal.get().getKey().getStringValue() : null;
	}
	
	
	private void setCurrentUserByKey(String userKey) {
		if(userKey != null) AuthenticatedUserThreadLocal.set(userAcc.getUserByKey(new UserKey(userKey)));
	}
}
