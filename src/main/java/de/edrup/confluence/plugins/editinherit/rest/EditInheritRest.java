package de.edrup.confluence.plugins.editinherit.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import de.edrup.confluence.plugins.editinherit.util.EditInheritHelper;


@Path("/")
public class EditInheritRest {
	
	private final EditInheritHelper editInheritHelper;
	private final PermissionManager permissionMan;
	private final PageManager pageMan;
	
	
	@Inject
	public EditInheritRest(EditInheritHelper editInheritHelper,  @ComponentImport PermissionManager permissionMan,
		@ComponentImport PageManager pageMan) {
		this.editInheritHelper = editInheritHelper;
		this.permissionMan = permissionMan;
		this.pageMan = pageMan;
	}
	
	
	@PUT
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/{pageId}/set")
	public Response set(@PathParam("pageId") String pageId, @QueryParam("mode") String mode) {
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		Page p = pageMan.getPage(Long.parseLong(pageId));
		if(p == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if(!permissionMan.hasPermission(user, Permission.SET_PERMISSIONS, p)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		boolean success = editInheritHelper.applyInheritance(Long.parseLong(pageId), Integer.parseInt(mode));
		return Response.ok(success ? "{\"status\":\"running\"}" : "{\"status\":\"blocked\"}").build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{pageId}/getStatus")
	public Response getStatus(@PathParam("pageId") String pageId) {
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		Page p = pageMan.getPage(Long.parseLong(pageId));
		if(p == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if(!permissionMan.hasPermission(user, Permission.VIEW, p)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		return Response.ok(editInheritHelper.getStatus(Long.parseLong(pageId))).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getAffected")
	public Response getAffected() {
		return Response.ok(editInheritHelper.getAffectedPages().toString()).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getAll")
	public Response getAll(@DefaultValue("") @QueryParam("spaceKeys") String spaceKeys) {
		if(permissionMan.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get())) {
			return Response.ok(editInheritHelper.getAllInheritances(spaceKeys).toString()).cacheControl(getNoStoreNoCacheControl()).build();
		}
		else {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
	
	
	private CacheControl getNoStoreNoCacheControl() {
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		cc.setNoStore(true);
		cc.setMustRevalidate(true);
		return cc;
	}
}
