package de.edrup.confluence.plugins.editinherit.ao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import de.edrup.confluence.plugins.editinherit.util.SplitList;
import net.java.ao.DBParam;
import net.java.ao.Query;

@Named
public class InheritanceService {
	
	private final ActiveObjects ao;
	private final int MAX_IN = 100;
	
	
	@Inject
	public InheritanceService(@ComponentImport ActiveObjects ao) {
		this.ao = ao;
	}
	
	
	public boolean addOrUpdateInheritance(Long pageId, Integer mode, String editPermissions) {
		Inheritance inheritance = null;
		if(hasInheritance(pageId)) {
			inheritance = ao.find(Inheritance.class, Query.select().where("PAGE_ID = ?", pageId))[0];
			inheritance.setMode(mode);
			inheritance.setEditPermissions(editPermissions);
		}
		else {
			inheritance = ao.create(Inheritance.class, new DBParam("PAGE_ID", pageId), new DBParam("MODE", mode), new DBParam("EDIT_PERMISSIONS", editPermissions));
		}
		inheritance.save();
		return true;
	}
	
	
	public boolean removeInheritance(Long pageId) {
		ao.delete(ao.find(Inheritance.class, Query.select().where("PAGE_ID = ?", pageId)));
		return true;
	}
	
	
	public boolean hasInheritance(Long pageId) {
		return getInheritance(pageId) != null;
	}
	
	
	public Inheritance getInheritance(Long pageId) {
		Inheritance[] inheritances = ao.find(Inheritance.class, Query.select().where("PAGE_ID = ?", pageId));
		if(inheritances.length > 0) {
			return inheritances[0];
		}
		else {
			return null;
		}
	}
	
	
	public List<Long> getInheritagesOfList(List<Long> pageIds) {
		ArrayList<Long> results = new ArrayList<Long>();
		List<List<Long>> batches = SplitList.partition(pageIds, MAX_IN);
		for(List<Long> batch : batches) {
			results.addAll(Arrays.stream(ao.find(Inheritance.class, Query.select().where("PAGE_ID IN (" + buildPlaceholder(batch.size()) + ")", buildParametersList(batch.toArray())))).map(x -> x.getPageId()).collect(Collectors.toList()));
		}
		return results;
	}
	
	
	public List<Inheritance> getAllInheritances() {
		return Arrays.asList(ao.find(Inheritance.class, Query.select()));
	}
	
	
	public void removeInheritancesOfList(List<Long> pageIds) {
		List<List<Long>> batches = SplitList.partition(pageIds, MAX_IN);
		for(List<Long> batch : batches) {
			ao.delete(ao.find(Inheritance.class, Query.select().where("PAGE_ID IN (" + buildPlaceholder(batch.size()) + ")", buildParametersList(batch.toArray()))));
		}
	}
	
	
	private Object[] buildParametersList(Object[] inArray) {
		List<Object> parameters = new ArrayList<Object>();
		for(Object o : inArray) {
			parameters.add(o);
		}
		return parameters.toArray();
	}
	
	
	private static String buildPlaceholder(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Size must be greater than 0");
        }
        StringBuilder placeholder = new StringBuilder();
        for (int i = 0; i < size - 1; i++) {
            placeholder.append("?, ");
        }
        placeholder.append("?");
        return placeholder.toString();
    }
}
