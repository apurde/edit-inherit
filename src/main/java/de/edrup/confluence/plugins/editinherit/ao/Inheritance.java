package de.edrup.confluence.plugins.editinherit.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

@Preload
public interface Inheritance extends Entity {
	
	@NotNull
	void setPageId(Long pageId);
	Long getPageId();
	
	@NotNull
	void setMode(Integer mode);
	Integer getMode();
	
	@NotNull
	@StringLength(value=StringLength.UNLIMITED)
	void setEditPermissions(String editPermissions);
	String getEditPermissions();
}
