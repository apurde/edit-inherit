package de.edrup.confluence.plugins.editinherit.util;

import java.util.ArrayList;
import java.util.List;

public class SplitList {

	public static <T> List<List<T>> partition(List<T> listToSplit, int size) {
		List<List<T>> splitList = new ArrayList<List<T>>();
		int start = 0;
		while(start < listToSplit.size()) {
			int end = start + size;
			if(end > listToSplit.size()) {
				end = listToSplit.size();
			}
			splitList.add(listToSplit.subList(start, end));
			start += size;
		}
		return splitList;
	}
}
