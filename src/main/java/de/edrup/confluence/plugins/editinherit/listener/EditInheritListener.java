package de.edrup.confluence.plugins.editinherit.listener;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageMoveEvent;
import com.atlassian.confluence.event.events.content.page.PageRemoveEvent;
import com.atlassian.confluence.event.events.content.page.PageTrashedEvent;
import com.atlassian.event.api.EventListener;

import de.edrup.confluence.plugins.editinherit.util.EditInheritHelper;


@Named
public class EditInheritListener {
	
	private final EditInheritHelper editInheritHelper;
	
	
	@Inject
	public EditInheritListener(EditInheritHelper editInheritHelper) {
		this.editInheritHelper = editInheritHelper;
	}
	
	
	@EventListener
	public void onPageCreateEvent(PageCreateEvent event) {
		editInheritHelper.handleAdd(event.getContent().getId());
	}
	
	
	@EventListener
	public void onPageMoveEvent(PageMoveEvent event) {
		if(!event.isMovedBecauseOfParent()) {
			editInheritHelper.handleMove(event.getPage().getId());
		}
	}
	
	
	@EventListener
	public void onPageTrashedEvent(PageTrashedEvent event) {
		editInheritHelper.handlePageRemove(event.getPage().getId());
	}
	

	@EventListener
	public void onPageRemoveEvent(PageRemoveEvent event) {
		editInheritHelper.handlePageRemove(event.getPage().getId());		
	}
}
