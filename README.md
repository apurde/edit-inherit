# Edit Permission Inheritance app
The Edit Permission inheritance app allows edit permission inheritance for Atlassian's Confluence.

## License
Please refer to our [Source code license agreement](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement)

## Manual
Please refer to the Wiki pages of this repository.

## Branches
The sources contain two branches. As the "dev" branch is work in progress you should only use the sources of the master branch.

## Building

Run the following commands on the sources (you need Atlassian's SDK for that):

```
atlas-clean
atlas-package
```  